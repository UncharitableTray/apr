extern crate rand;

use crate::unit::Unit;

#[derive(Clone, Debug, PartialEq)]
pub struct Population<G: Unit> {
    population: Vec<G>,
}

impl<G: Unit> Population<G> {
    pub fn from_units(units: Vec<G>) -> Population<G> {
        Population { population: units }
    }

    pub fn units(&self) -> &[G] {
        &self.population
    }

    pub fn units_mut(&mut self) -> &mut Vec<G> {
        &mut self.population
    }

    pub fn size(&self) -> usize {
        self.population.len()
    }

    /// Sorts the vector of units in an ascending order of their fitness values.
    pub fn sort_ascending(&mut self) {
        let units = &mut self.population;
        units.sort_by(|a, b| {
            a.get_fitness_scalar()
                .partial_cmp(&b.get_fitness_scalar())
                .unwrap()
        });
    }

    /// Sorts the vector of units in an ascending order of their fitness values.
    pub fn sort_descending(&mut self) {
        let units = &mut self.population;
        units.sort_by(|a, b| {
            b.get_fitness_scalar()
                .partial_cmp(&a.get_fitness_scalar())
                .unwrap()
        });
    }

    /// Normalizes the fitnesses such that the lowest fitness
    /// is subtracted from all other fitnesses.
    /// The side-effect of this method is that the population
    /// is sorted by fitness values in an ascending order.
    pub fn sort_normalize(&mut self) {
        self.sort_ascending();
        let worst = self
            .population
            .get(self.population.len() - 1)
            .unwrap()
            .get_fitness_scalar();

        for unit in self.population.iter_mut() {
            let their = unit.get_fitness_scalar();
            unit.update_fitness(their - worst);
        }
    }

    pub fn fitness_sum(&self) -> f64 {
        self.population
            .iter()
            .fold(0.0, |acc, x| acc + x.get_fitness_scalar())
    }
}
