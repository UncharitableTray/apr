import numpy as np
import sys
import matplotlib.pyplot as plt

filename = sys.argv[1]
lines = []
matrices = []

with open(filename, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split(' ')
            nums = list(map(float, splitter))
            lines.append(nums)
        else:
            arr = np.array(lines)
            #print(arr)
            matrices.append(arr)
            lines = []

fig = plt.subplots(2, 3, figsize=(10, 10))
methods = ['Euler', 'Inverse Euler', 'Trapezoidal', 'RK4', 'PE(CE)2', 'PECE']
ctr = 1

#print(matrices)

for m in matrices:
    ax = plt.subplot(2, 3, ctr)
    ax.set(title=methods[ctr-1])
    ax.plot(m[:, 0], m[:, 1])
    ax.plot(m[:, 0], m[:, 2])
    ctr += 1

plt.savefig("img_" + filename[:-4])
plt.show()
