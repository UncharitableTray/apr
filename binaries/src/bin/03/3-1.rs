extern crate linalg;
extern crate optimi;

use linalg::vector::RealVec;
use optimi::functions::*;
use optimi::methods::*;

pub fn main() {
    let x0 = vec![0.0, 0.0];
    let x0 = RealVec::from_vec(x0);

    let mut f = FnCaller::with_gradients(lab3_fn3, lab3_der3, lab3_hes3);

    let x_best = gradient_descent(&mut f, &x0, 0.2, false, 1e-6).unwrap();
    println!("Best: {}", x_best);
    f.print_calls();
    f.reset_count();
    println!();

    let x_best = gradient_descent(&mut f, &x0, 0.2, true, 1e-6).unwrap();
    println!("Best: {}", x_best);
    f.print_calls();
    f.reset_count();
}
